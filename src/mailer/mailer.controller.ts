import { MailerService as NestMailerService } from '@nestjs-modules/mailer';
import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { MailerService } from './mailer.service';

@Controller()
export class MailerController {
  constructor(private readonly nestMailerService: NestMailerService, private readonly mailerService: MailerService) {}

  @MessagePattern('send-invitation')
  async handleSendInvitation(@Payload() message) {
    const token = message.headers.accessToken;
    const email = message.value.email as string;
    const companyName = message.value.companyName as string;
    const url = `https://internium.monkeyhackers.org/auth/company-user/?access-token=${token}`;

    await this.nestMailerService.sendMail({
      to: email,
      template: '/company-invitation',
      subject: `Приглашение в ${companyName}`,
      context: {
        url,
        companyName,
      },
    });
  }

  @MessagePattern('vacancy-invitation')
  async handleSendEmailNotification(@Payload() message) {
    await this.mailerService.sendVacancyInvitation(
      message.value.internId as number,
      message.value.vacancyId as number,
      message.value.message as string,
    );
  }
}
