import { Module } from '@nestjs/common';
import { MailerModule as NestMailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { MailerController } from './mailer.controller';
import { ConfigService } from '@nestjs/config';
import { MailerService } from './mailer.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InternEntity } from '@/entities/intern.entity';
import { VacancyEntity } from '@/entities/vacancy.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([InternEntity, VacancyEntity]),
    NestMailerModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        transport: configService.get<string>('MAIL_TRANSPORT_CREDENTIALS'),
        defaults: {
          from: '"internium" <internium.no-reply@yandex.ru>',
        },
        template: {
          dir: process.cwd() + '/templates',
          adapter: new PugAdapter(),
          options: {
            strict: true,
          },
        },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [MailerController],
  exports: [MailerService],
  providers: [MailerService],
})
export class MailerModule {}
