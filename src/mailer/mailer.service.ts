import { InternEntity } from '@/entities/intern.entity';
import { VacancyEntity } from '@/entities/vacancy.entity';
import { MailerService as NestMailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MailerService {
  constructor(
    private readonly nestMailerService: NestMailerService,
    @InjectRepository(InternEntity) private readonly internRepository: Repository<InternEntity>,
    @InjectRepository(VacancyEntity) private readonly vacancyRepository: Repository<VacancyEntity>,
  ) {}

  async sendVacancyInvitation(internId: number, vacancyId: number, message: string) {
    const vacancy = await this.vacancyRepository.findOne({ where: { id: vacancyId } });
    const intern = await this.internRepository.findOne({ where: { id: internId } });
    const url = `https://internium.monkeyhackers.org/auth/company-user/?access-token=`;

    await this.nestMailerService.sendMail({
      to: intern.email,
      template: '/vacancy-invitation',
      subject: `Приглашение на вакансию ${vacancy.title}`,
      context: {
        url,
        firstName: intern.firstName,
        vacancyName: vacancy.title,
      },
    });
  }
}
