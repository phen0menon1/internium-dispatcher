FROM node:14.18

RUN apt update && apt install --no-install-recommends -y curl ca-certificates

WORKDIR /app/internium

COPY .env package.json yarn.lock nest-cli.json tsconfig.json tsconfig.build.json ./
COPY prisma ./prisma/
COPY ./src ./src

RUN yarn cache clean --all
RUN yarn
RUN npx prisma generate 

EXPOSE 8100

CMD yarn start:dev